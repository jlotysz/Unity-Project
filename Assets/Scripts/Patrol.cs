using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : MonoBehaviour
{
    [SerializeField]
    List<GameObject> myTargets = new List<GameObject>();

    private Animator myAnimator;
    private NavMeshAgent myAgent;
    private int myTargetIndex = 0;

    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        myAgent.SetDestination(myTargets[myTargetIndex].transform.position);

        myAnimator = GetComponent<Animator>();
        myAnimator.SetBool("IsRunning", true);
    }

    void Update()
    {
        if (myAgent.remainingDistance < myAgent.stoppingDistance)
        {
            myTargetIndex = (myTargetIndex + 1) % myTargets.Count;
            SetAgentDestination();
        }
    }

    private void SetAgentDestination()
    {
        myAgent.SetDestination(myTargets[myTargetIndex].transform.position);
    }
}
