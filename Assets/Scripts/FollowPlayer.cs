using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField]
    Transform target = null;

    [SerializeField]
    Vector3 offset = Vector3.zero;

    void Update()
    {
        transform.position = target.position + offset;
    }
}
