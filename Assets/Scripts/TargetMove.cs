using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour
{
    void Update()
    {
        var x = Input.GetAxis("Horizontal");
        var z = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(x, 0, z) * Time.deltaTime * 5;
        transform.Translate(movement);
    }
}
