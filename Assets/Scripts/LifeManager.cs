using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class LifeManager : MonoBehaviour
{
    Animator myAnimator = null;
    PlayerMove myPlayerMove = null;

    [SerializeField]
    [Tooltip("Drain Cooldown (default 1s)")]
    float defaultDrainCooldown = 1f;

    [SerializeField]
    [Tooltip("Gain Cooldown (default 0.1s)")]
    float defaultGainCooldown = 0.1f;



    [Header("Blood")]
    [SerializeField]
    Slider bloodSlider = null;

    [SerializeField]
    GameObject bloodRefiller = null;

    [SerializeField]
    [Tooltip("Drain per Second")]
    float bloodDrainSpeed = 12f;

    [SerializeField]
    [Tooltip("Gain per 1/10 Second")]
    float bloodGainSpeed = 10f;

    float bloodAmount = 100;



    [Header("Food")]
    [SerializeField]
    Slider foodSlider = null;

    [SerializeField]
    GameObject foodRefiller = null;

    [SerializeField]
    [Tooltip("Drain per Second")]
    float foodDrainSpeed = 17f;

    [SerializeField]
    [Tooltip("Gain per 1/10 Second")]
    float foodGainSpeed = 10f;

    float foodAmount = 100;



    [Header("Sacrifice")]
    [SerializeField]
    Slider sacrificeSlider = null;

    [SerializeField]
    GameObject sacrificeRefiller = null;

    [SerializeField]
    [Tooltip("Drain per Second")]
    float sacrificeDrainSpeed = 5f;

    [SerializeField]
    [Tooltip("Gain per 1/10 Second")]
    float sacrificeGainSpeed = 10f;

    float sacrificeAmount = 100;



    [Header("Sleep")]
    [SerializeField]
    Slider sleepSlider = null;

    [SerializeField]
    GameObject sleepRefiller = null;

    [SerializeField]
    [Tooltip("Drain per Second")]
    float sleepDrainSpeed = 10f;

    [SerializeField]
    [Tooltip("Gain per 1/10 Second")]
    float sleepGainSpeed = 10f;

    float sleepAmount = 100;



    private void Awake()
    {
        myPlayerMove = GetComponent<PlayerMove>();
        myAnimator = GetComponent<Animator>();

        bloodSlider.value = bloodAmount;
        StartCoroutine(StartDrainingBlood());
        StartCoroutine(StartDrainingFood());
        StartCoroutine(StartDrainingSacrifice());
        StartCoroutine(StartDrainingSleep());
    }

    public void RefillResource(int type)
    {
        switch (type)
        {
            case (int)GameManager.ItemType.Blood:
                {
                    StartCoroutine(StartGainingBlood());
                    break;
                }
            case (int)GameManager.ItemType.Food:
                {
                    StartCoroutine(StartGainingFood());
                    break;
                }
            case (int)GameManager.ItemType.Sacrifice:
                {
                    StartCoroutine(StartGainingSacrifice());
                    break;
                }
            case (int)GameManager.ItemType.Sleep:
                {
                    StartCoroutine(StartGainingSleep());
                    break;
                }
        }
    }

    IEnumerator StartDrainingBlood()
    {
        myPlayerMove.IsWaiting = false;
        while (bloodSlider.value > 0)
        {
            yield return new WaitForSeconds(defaultDrainCooldown);
            bloodAmount = Mathf.Clamp(bloodAmount -= bloodDrainSpeed, 0, 100);
            bloodSlider.value = bloodAmount;

            //bloodSlider.value = Mathf.Lerp(bloodSlider.value, bloodAmount, 0.1f);

            //var currentSliderValue = Mathf.SmoothDamp(bloodAmount, bloodSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //bloodSlider.value = currentSliderValue;
        }
        myPlayerMove.QueueNewTarget(bloodRefiller);
    }

    IEnumerator StartGainingBlood()
    {
        myAnimator.SetBool("IsDrinking", true);
        while (bloodSlider.value < 100)
        {
            yield return new WaitForSeconds(defaultGainCooldown);
            bloodAmount = Mathf.Clamp(bloodAmount += bloodGainSpeed, 0, 100);
            bloodSlider.value = bloodAmount;

            //var currentSliderValue = Mathf.SmoothDamp(bloodAmount, bloodSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //bloodSlider.value = currentSliderValue;
        }
        myAnimator.SetBool("IsDrinking", false);
        StartCoroutine(StartDrainingBlood());
        myPlayerMove.SetNextQueuedTarget();
    }

    IEnumerator StartDrainingFood()
    {
        myPlayerMove.IsWaiting = false;
        while (foodSlider.value > 0)
        {
            yield return new WaitForSeconds(defaultDrainCooldown);
            foodAmount = Mathf.Clamp(foodAmount -= foodDrainSpeed, 0, 100);
            foodSlider.value = foodAmount;

            //var currentSliderValue = Mathf.SmoothDamp(foodAmount, foodSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //foodSlider.value = currentSliderValue;
        }
        myPlayerMove.QueueNewTarget(foodRefiller);
    }

    IEnumerator StartGainingFood()
    {
        myAnimator.SetBool("IsEating", true);
        while (foodSlider.value < 100)
        {
            yield return new WaitForSeconds(defaultGainCooldown);
            foodAmount = Mathf.Clamp(foodAmount += foodGainSpeed, 0, 100);
            foodSlider.value = foodAmount;

            //var currentSliderValue = Mathf.SmoothDamp(foodAmount, foodSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //foodSlider.value = currentSliderValue;
        }
        myAnimator.SetBool("IsEating", false);
        StartCoroutine(StartDrainingFood());
        myPlayerMove.SetNextQueuedTarget();
    }

    IEnumerator StartDrainingSacrifice()
    {
        myPlayerMove.IsWaiting = false;
        while (sacrificeSlider.value > 0)
        {
            yield return new WaitForSeconds(defaultDrainCooldown);
            sacrificeAmount = Mathf.Clamp(sacrificeAmount -= sacrificeDrainSpeed, 0, 100);
            sacrificeSlider.value = sacrificeAmount;

            //var currentSliderValue = Mathf.SmoothDamp(sacrificeAmount, sacrificeSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //sacrificeSlider.value = currentSliderValue;
        }
        myPlayerMove.QueueNewTarget(sacrificeRefiller);
    }

    IEnumerator StartGainingSacrifice()
    {
        myAnimator.SetBool("IsSacrificing", true);
        while (sacrificeSlider.value < 100)
        {
            yield return new WaitForSeconds(defaultGainCooldown);
            sacrificeAmount = Mathf.Clamp(sacrificeAmount += sacrificeGainSpeed, 0, 100);
            sacrificeSlider.value = sacrificeAmount;

            //var currentSliderValue = Mathf.SmoothDamp(sacrificeAmount, sacrificeSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //sacrificeSlider.value = currentSliderValue;
        }
        myAnimator.SetBool("IsSacrificing", false);
        StartCoroutine(StartDrainingSacrifice());
        myPlayerMove.SetNextQueuedTarget();
    }

    IEnumerator StartDrainingSleep()
    {
        myPlayerMove.IsWaiting = false;
        while (sleepSlider.value > 0)
        {
            yield return new WaitForSeconds(defaultDrainCooldown);
            sleepAmount = Mathf.Clamp(sleepAmount -= sleepDrainSpeed, 0, 100);
            sleepSlider.value = sleepAmount;

            //var currentSliderValue = Mathf.SmoothDamp(sleepAmount, sleepSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //sleepSlider.value = currentSliderValue;
        }
        myPlayerMove.QueueNewTarget(sleepRefiller);
    }

    IEnumerator StartGainingSleep()
    {
        myAnimator.SetBool("IsSleeping", true);
        while (sleepSlider.value < 100)
        {
            yield return new WaitForSeconds(defaultGainCooldown);
            sleepAmount = Mathf.Clamp(sleepAmount += sleepGainSpeed, 0, 100);
            sleepSlider.value = sleepAmount;

            //var currentSliderValue = Mathf.SmoothDamp(sleepAmount, sleepSlider.value, ref slidersVelocity, 100 * Time.deltaTime);
            //sleepSlider.value = currentSliderValue;
        }
        myAnimator.SetBool("IsSleeping", true);
        StartCoroutine(StartDrainingSleep());
        myPlayerMove.SetNextQueuedTarget();
    }
}
