using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMove : MonoBehaviour
{
    Queue<GameObject> queueTargets = new Queue<GameObject>();
    LifeManager lifeManager;
    Animator myAnimator;
    NavMeshAgent myAgent;

    private bool isWaiting = false;
    public bool IsWaiting
    {
        get { return isWaiting; }
        set { isWaiting = value; }
    }

    void Awake()
    {
        lifeManager = GetComponent<LifeManager>();
    }

    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();

        myAnimator = GetComponent<Animator>();
        myAnimator.SetBool("IsRunning", false);
    }

    void Update()
    {
        if (myAgent.remainingDistance < myAgent.stoppingDistance && myAnimator.GetBool("IsRunning"))
        {
            TryGetNewTarget();
        }

        var canDance = queueTargets.Count <= 0 && !(myAnimator.GetBool("IsDrinking") || myAnimator.GetBool("IsEating") || myAnimator.GetBool("IsSacrificing") || myAnimator.GetBool("IsSleeping"));
        if (canDance)
        {
            StartDance();
        }
        else
        {
            StopDance();
        }
    }

    private void TryGetNewTarget()
    {
        isWaiting = true;
        myAnimator.SetBool("IsRunning", false);
        myAgent.ResetPath();

        GameObject currentTarget = queueTargets.Dequeue();
        if (currentTarget == null)
        {
            return;
        }
        int type = currentTarget.GetComponent<TargetType>().myType;

        lifeManager.RefillResource(type);
    }

    public void QueueNewTarget(GameObject target)
    {
        queueTargets.Enqueue(target);

        if (queueTargets.Count == 1 && !myAgent.pathPending)
        {
            SetNextQueuedTarget();
        }
    }

    public void SetNextQueuedTarget()
    {
        if (queueTargets.Count > 0 && !isWaiting)
        {
            myAnimator.SetBool("IsRunning", true);
            myAgent.SetDestination(queueTargets.Peek().transform.position);
        }
    }

    private void StartDance()
    {
        // Debug.Log("Dance");
        myAnimator.SetBool("IsDancing", true);
    }

    private void StopDance()
    {
        // Debug.Log("Dancen't");
        myAnimator.SetBool("IsDancing", false);
    }
}
