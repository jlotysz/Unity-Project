using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameManager
{
    public enum ItemType
    {
        Nothing = 0,
        Blood = 1,
        Food = 2,
        Sacrifice = 3,
        Sleep = 4
    }
}
